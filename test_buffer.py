# -*- coding: utf-8 -*-
"""
Testing buffer
"""

import numpy as np
import pytest

@pytest.fixture
def short_buffer():
    from buffer import buffer
    b = buffer(10)
    data = np.array([1,2,3])
    for i in range(12):
        b.add(data)
    
    return b
        
def test_create_buffer():
    """
    This test simply creates a buffer for 1000 points and 1 sensor
    """
    from buffer import buffer
    b = buffer(1000,1)
    
    #%%
def test_short_buffer_add(short_buffer):
    """
    This test creates a buffer for 10 points and 1 sensor.  Then, adds 30
    points to the buffer.
    """

    expected_buffer = np.array([[ 1.], [ 2.], [ 3.], [ 1.], [ 2.],[ 3.], [ 3.], [ 1.],[ 2.],[ 3.]])
    assert np.array_equal(expected_buffer, short_buffer.__buffer__)

def test_short_buffer_read(short_buffer):
    """
    This tests the reading of the short buffer.
    """

    expected_buffer = np.array([[3.], [ 1.], [ 2.], [ 3.], [ 1.], [ 2.],[ 3.], [ 1.],[ 2.],[ 3.]])
    assert np.array_equal(expected_buffer, short_buffer.read())
    
