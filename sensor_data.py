"""
This script creates a json file that contains the parameters of the sensors
used for the DAQ.  The output file is sensors.json
"""
import json

# data for all sensors
sen = {"393B05":
        {"47085":  # ASSET sensor
            {"sensitivity": 9980,
             "sensitivity_units": 'mV/g',
             "type": 'Accelerometer',
             "units": 'g',
             "bias_level" : 11.4,
             "bias_level_units":"VDC"},
        "46978":  # ASSET sensor
            {"sensitivity": 10030,
             "sensitivity_units": 'mV/g',
             "type": 'Accelerometer',
             "units": 'g',
             "bias_level" : 11.4,
             "bias_level_units":"VDC"},
        "47083":
            {"sensitivity": 10020,
             "sensitivity_units": 'mV/g',
             "type": 'Accelerometer',
             "units": 'g',
             "bias_level" : 11.4,
             "bias_level_units":"VDC"},
         "46339":
             {"sensitivity": 10300,
              "sensitivity_units": 'mV/g',
              "type":"Accelerometer",
              "units": 'g',
              "bias_level": 11.5,
              "bias_level_units": "VDC"}
                },
       "3701D1FA20G":       #model number
          {"7707":              #serial number
            {"sensitivity": 100.4,      
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "7708":              #serial number
            {"sensitivity": 100.1,       
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "7709":              #serial number              
            {"sensitivity": 99.2,        
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "7710":              #serial number
            {"sensitivity": 101.0,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "7711":              #serial number
            {"sensitivity": 99.2,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            }
            },
       "333B50":            #model number
          {"40789":             #serial number
            {"sensitivity": 1054,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "39833":             #serial number
            {"sensitivity": 995,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "39831":             #serial number
            {"sensitivity": 1019,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "40787":             #serial number
            {"sensitivity": 1035,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "40790":             #serial number
            {"sensitivity": 1062,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "39832":             #serial number
            {"sensitivity": 1001,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "40788":             #serial number
            {"sensitivity": 1084,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "40786":             #serial number
            {"sensitivity": 1048,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "LW51249":           #serial number
            {"sensitivity": 982,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "LW51250":           #serial number
            {"sensitivity": 1019,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "LW51384":           #serial number
            {"sensitivity": 995,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            },
           "LW51385":        #serial number
            {"sensitivity": 1019,
             "sensitivity_units": "mV/g",
             "type":"Accelerometer",
             "units":"g"
            }
            },
            
       "393B31":            #model number
          {"51814":         #serial number
               {"sensitivity":9950,
                "sensitivity_units":"mV/g",
                "type":"Accelerometer",
                "units":"g",
                "bias_level":11500,
                "bias_level_units":"mVDC",
                "model":"M393B31",
                "owner":"SFSU",
                },
           "51819":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9710,
                "bias_level_units":"mVDC",
                "bias_level":11800,
                "units":"g",
                "owner":"SFSU",
                },
           "51835":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9770,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11400,
                "owner":"SFSU",
                },
           "51836":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9730,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11300,
                "owner":"SFSU",
                },
           "51820":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9940,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11800,
                "owner":"SFSU",
                },
           "51815":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9890,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11500,
                "owner":"SFSU",
                },
           "58593":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9770,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11400,
                "owner":"UofSC",
                },
           "58293":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9950,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11200,
                "owner":"UofSC",
                },
           "58290":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9750,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11000,
                "owner":"UofSC",
                },
           "58288":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9770,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11000,
                "owner":"UofSC",
                },
           "58291":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9770,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11000,
                "owner":"UofSC",
                },
           "58292":         #serial number
               {"type":"Accelerometer",
                "sensitivity_units":"mV/g",
                "sensitivity":9960,
                "units":"g",
                "bias_level_units":"mVDC",
                "bias_level":11100,
                "owner":"UofSC",
                },                
               },
               
       "M3711B1110G":        #model number
          {"SLW8813":        #serial number
            {"sensitivity":198,
             "sensitivity_units":"mV/g",
             "type":"Accelerometer",
             "units":"g",
             "bias_level":-1.9,
             "bias_level_units":"mVDC",
             "owner":"SFSU",
             }
            },
       "M086D05":            #model number
           {"S36630":        #serial number
            {"type":"Hammer",
             "extenders":"steel_extender",
             "sensitivity_units":"mV/N",
             "sensitivity":0.23,
             "units":"N",
             "owner":"SFSU",
             }
            },
            
       "M086D20":            #model number
           {"S40304":        #serial number
               {"type":"Hammer",
                "extenders":"no_extender",
                "sensitivity_units":"mV/N",
                "sensitivity":0.2508,
                "units":"N",
                "owner":"SFSU",
                }
               },

       "130F20":            #model number
          {"51061":            #serial number
            {"sensitivity": 40.2,
             "sensitivity_units": "mV/Pa",
             "type":"Sound",
             "units":"Pa"
            }
            },
       "208C03":            #model number
          {"LW48153":            #serial number
            {"sensitivity": 2.414,
             "sensitivity_units": "mV/N",
             "type":"Force",
             "units":"N"
            },
           "LW48154":            #serial number
            {"sensitivity": 2.355,
             "sensitivity_units": "mV/N",
             "type":"Force",
             "units":"N"
            },
           "LW48155":            #serial number 
            {"sensitivity": 4.405,
             "sensitivity_units": "mV/N",
             "type":"Force",
             "units":"N"
            },
           "LW48161":            #serial number
            {"sensitivity": 2.385,
             "sensitivity_units": "mV/N",
             "type":"Force",
             "units":"N"
            }
            },
       "086C03":            #model number
          {"23410":              #serial number
            {"type":"Hammer",
             "units":"N",
             "no_extender": 
              {"sensitivity": 2.22,
               "sensitivity_units": "mV/N"
              },
             "steel_extender":
              {"sensitivity": 2.33,
               "sensitivity_units": "mV/N"
              }
            }
          },
       "086D50":            #model number
          {"31296":              #serial number
            {"type":"Hammer",
             "units":"N",
             "no_extender":
              {"sensitivity": 2.22,
               "sensitivity_units": "mV/N"
              },
             "steel_extender":
              {"sensitivity": 2.33,
               "sensitivity_units": "mV/N"
              }
            }   
         }
      }

with open('sensors.json', 'w') as outfile:
    json.dump(sen, outfile)
    
