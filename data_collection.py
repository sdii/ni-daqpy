# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 14:14:59 2018

@author: Data
"""

# Importing packages
import nidaqmx
import matplotlib.pyplot as plt
import numpy as np
from buffer import buffer as buffer
import scipy as sp
from datetime import datetime as datetime
import os
import tables

#%% Constants
fs = 1652 #Sampling frequency in Hz
record_length = 20  # Record length in seconds
pre_trigger = 5  # Pre-trigger in seconds
post_trigger = record_length - pre_trigger
trigger = 0.001     # Trigger value
data_folder = 'data'

#%% Defining pytables event
class Event(tables.IsDescription):
    date = tables.Time32Col()
    # To make the time stamp this might work int((datetime.now() - datetime(1970,1,1)).total_seconds())

#%% Creating the hd5 file and structure
file_name = '%s/gait_data.hd5'%(data_folder)
h5file = tables.open_file(file_name, mode = 'w', title='Gait data')
# Creating a new group - like a directory
exp_records = h5file.create_group('/','experimental_data','Experimental records')
# Create a table
dates_table = h5file.create_table(exp_records, 'dates',Event,'Dates of records')
event = dates_table.row
# Records of acceleration are created in an earray
accels = h5file.create_earray(exp_records,'data',tables.Float64Atom(), shape=(0,record_length*fs))
accels.attrs.units = 'g'
accels.attrs.sampling_frequency = fs
accels.attrs.pre_trigger = pre_trigger
accels.pre_trigger_units = 's'
accels.trigger = trigger

#%% Create a buffer
b = buffer((record_length+2)*fs,1)

#%% Collect data    
system = nidaqmx.system.System.local()
for device in system.devices:
    print  (device)
    
flag = False

try:
    with nidaqmx.Task() as task:
        #% Create a channel
        task.ai_channels.add_ai_accel_chan(physical_channel = "cDAQ2Mod2/ai3", sensitivity = 10000, min_val = -0.5, max_val = 0.5)
        task.timing.cfg_samp_clk_timing(fs, samps_per_chan=fs,)    
        #print task.timing.samp_clk_rate(0)
        while True:
            data = task.read(number_of_samples_per_channel=1652)
            b.add(data)
            if max(np.abs(data)) > trigger and not flag:
                flag = True
                frames_remaining = np.ceil(post_trigger) # Ramaining frames to read
            if flag:
                if frames_remaining == 0: # If we read all remaining frames
                    record = b.read()
                    trigger_location = np.argmax(np.abs(record[fs:]) > trigger)
                    trigger_location += fs
                    if trigger_location == 0:
                        import sys
                        sys.exit("aa! errors!")
                    start_record = trigger_location - np.round(fs*pre_trigger)
                    end_record = start_record + (record_length * fs)
                    print ("Trigger location = %i"%trigger_location)
                    print ("start_record = %i"%start_record)
                    print ("end_record = %i"%end_record)
                    record = record[start_record:end_record]
                    t = np.array(range(len(record)))/float(fs)
                    plt.clf()
                    plt.plot(t,record)
                    plt.pause(0.1)
                    flag = False
                    # Create a new row in the pytables table and flush
                    # Make sure that the dimensions are correct
                    if record.shape[0] == accels.shape[1]:
                        event['date'] = int((datetime.now() - datetime(1970,1,1)).total_seconds())
                        event.append()
                        # Add data to the array of accelerations
                        accels.append(record.transpose())
                        h5file.flush()
                    else:
                        print ('Something happened - record not of the same length')
                frames_remaining -= 1

except KeyboardInterrupt:
    # Program stopped, save the data
    h5file.close()
    print ("The end")
