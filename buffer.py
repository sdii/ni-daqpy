# -*- coding: utf-8 -*-
"""
Buffer object for data collection
"""

# Import packages
import numpy as np

class buffer:
    pretrig = 0   # Number of points in the buffer for pre-trigger
    trigger = 0   # Amplitude used for trigger
    __buffer__ = np.array([])  # Buffer itself
    __position__ = 0 # Current position to add to the buffer
    
    def __init__(self,buffer_size, n_sensors = 1):
        """This class is an object to serve as a buffer for data collection.
        
        Args:
            buffer_size (int): Number of points for the buffer. Corresponds to the
                number of rows
            n_sensors (int): Number of sensors in the buffer.  Corresponds to the number 
                of columns.  Default value is 1.
                
        Attributes:
            buffer_size (int): Number of points for the buffer.
            n_sensors (int): Number of sensors for the buffer.
        
        """
        self.__buffer__ = np.zeros((buffer_size, n_sensors))
        self.buffer_size = buffer_size
        self.n_sensors = n_sensors
        
    def add(self,data):
        """Adds data to a buffer object.
        
        Args:
            data (np.array): Array to add to the buffer.  The size of data should
            be n by self.n_sensors where n < self.buffer_size
        """
        # Check dimmensions of data and expand if needed
        if len(np.shape(data)) == 1:
            data = np.expand_dims(data,axis=1)
        
        # Adding to the buffer
        if self.__position__ + np.shape(data)[0] > np.shape(self.__buffer__)[0]:
            # If the data is bigger than the remaining of the buffer
            points_end = np.shape(self.__buffer__)[0] - self.__position__
            points_beginning = np.shape(data)[0] - points_end
            self.__buffer__[self.__position__:,:] = data[0:points_end]
            self.__buffer__[0:points_beginning,:] = data[points_end:]
            self.__position__ = points_beginning
        else:    
            # If the data is smaller than the remainder of the buffer
            self.__buffer__[self.__position__:self.__position__+np.shape(data)[0],:] = data
            self.__position__ += np.shape(data)[0]

    def read(self):
        """ Reads the buffer
        
        Returns:
            data (np.array): Buffer in the correct order.
        """
        data = np.vstack((self.__buffer__[self.__position__:],self.__buffer__[0:self.__position__]))
        return data

#%%        
def plot_data(fpath,number):
    import scipy.io as io
    import matplotlib.pyplot as plt
    
    data = io.loadmat(fpath,variable_names = ['data%i'%number, 'fs','trigger'])
    t = np.array(range(len(data['data%i'%number])))/float(data['fs'])
    plt.plot(t,data['data%i'%number])    
    plt.plot ([t[0],t[-1]],[data['trigger'], data['trigger']],'r:')
    