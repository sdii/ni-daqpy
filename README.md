** Introduction **

The purpose of this repository is to develop a set of python scripts that are enable data collection and documenting the metadata of those experiments in an HDF5 file.  The scripts are for National Instrument data acquisition systems and focus in vibration type of data (accelerometers, force sensors, impact hammers, etc).

Feel free to leave your ideas/issues/etc in the "issues" link on the left hand side.

** Contributors **

Lani McGuire - Gobernor's School of Science and Mathematics (South Carolina)
Juan M. CAicedo - University of South Carolina

** License **

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License.](https://creativecommons.org/licenses/by-sa/4.0/)