"""
Collects all data pertaining to the experiment and will eventually store it in 
an HDF5 file.
"""

def identify_channel():
    """
    Identifies the channel of the sensor that is tapped.
    
    This function collects data from all the channels available in the DAQ
    connected to the computer.  The sensor channel is identified by calculating
    the maximum amplitude of the channel.  
    
    Returns
    -------
    channel : str
        The channel in the data acquisition system
        
    Notes
    -----
    The sensitivity is set to 1 for all channels.  Therefore, the incorrect
    channel can be identified if sensors of different sensitivity are connected.
    Data is collected at 1652Hz.
    """
    import nidaqmx
    import numpy as np
    
    system = nidaqmx.system.System.local()
    
    devices = []
    channels = []
    ranges = []
    fs = 1652
    
    with nidaqmx.Task() as task:
        
        for device in system.devices:
            d_name = device.name
            devices.append(d_name)
            for chan in device.ai_physical_chans:
                c_name = chan.name
                channels.append(c_name)
                
                task.ai_channels.add_ai_accel_chan(physical_channel = c_name, sensitivity = 1, min_val = -10, max_val = 10)
                
        task.timing.cfg_samp_clk_timing(fs, samps_per_chan=fs*5,)
        data = task.read(number_of_samples_per_channel=1652*5)
        for item in data:
            r = np.max(item) - np.min(item)
            ranges.append(r)
        
    # Identify the channel with the biggest range
    index = ranges.index(max(ranges))
    return channels[index]


class DAQ():
    """
    Collects and stores the parameters of the experiment and runs a DAQ system. 
    
    This class allows the user to setup a National Instruments DAQ system
    via a serie of questions.  This class was originally developed for
    experiments dealing with accelerometers.  Other type of sensors might need
    modifications to the code.  The class generates an HDF5 file with the
    appropriate metadata.
    
    Attributes
    ----------
    fs : float
        Sampling frequency in Hz (Default 1652 Hz)
    record_length : float
        Record length in seconds (Default 30 seconds)
    pre_trigger : float
        Time before trigger (Default 10 seconds)
    sensors : list
        List of dictionaries with information about each sensor.  Each
        dictionary has the following fields
        
        +-------------------+------------------------------------------+
        | model             | Sensor's model (str)                     |
        +-------------------+------------------------------------------+
        | serial            | Sensor's serial number (str)             |
        +-------------------+------------------------------------------+
        | sensitivity       | Sensor's sensitivity (float)             |
        +-------------------+------------------------------------------+
        | sensitivity_units | Sensitivity units (e.g. 'mV/g') (str)    |
        +-------------------+------------------------------------------+
        | location          | Location of the sensor. List of 3 floats |
        +-------------------+------------------------------------------+
        | location_units    | Location units (e.g. 'm')                |
        +-------------------+------------------------------------------+
        | direction         | Direction.  List of 3 floats             |
        +-------------------+------------------------------------------+
        | trigger           | Level that will trigger data collection  |
        +-------------------+------------------------------------------+
        
    parameters : dict
        List of parameters to be added to the HDF5.  The dictionary has two
        keys: 'general' and 'specific'.  General parameters are those that 
        describe the whole experiment such as a title for the experiment.
        Specific parameters are specific for each record.  This is useful when
        each record needs to have specific variables.  For example, when
        collecting data from human excitation and the age or weight of the
        person is to be collected and documented.
    title : str
        Experiment's title
    
        
    Methods
    -------
    collect_data(fname='data.hdf5')
        Collects data and saves the data and the metadata in the file 'fname'
    load_setup(fname='setup.json')
        Load configuration of experiment stored in file 'fname'
    save_setup(fname='setup.json')
        Saves the setup of the experiment and associated metadata in the file
        'fname'.
    setup()
        Asks the user a serie of questions to setup the DAQ
    
    
    """
    def __init__(self,title = '', fs = 1652, record_length = 30, pre_trigger = 10, sensors = [], parameters = {}):
        # Parameters
        self.fs = fs  # sampling frequency in Hz
        self.record_length = record_length  # Record length in seconds
        self.pre_trigger = pre_trigger    # Pre-trigger length in seconds
        self.sensors = sensors
        self.parameters = parameters
        self.title = title
        self.__buffer__ = 0
    
    def setup_daq(self):
        """
        Setup NI DAQ system and returns the task
        TODO: Complete help
        """
        # Importing packages
        import nidaqmx
        
        # Create the task
        task = nidaqmx.Task()
        
        #% Create the channels
        for sensor in self.sensors:
            if sensor['sensor_type'] == "Accelerometer":
                task.ai_channels.add_ai_accel_chan(physical_channel = sensor['channel'], sensitivity = sensor['sensitivity'], min_val = sensor['min_val'], max_val = sensor['max_val'])
            elif sensor['sensor_type'] == "Hammer" or sensor['sensor_type'] == "Force":
                task.ai_channels.add_ai_force_iepe_chan(physical_channel = sensor['channel'], sensitivity = sensor['sensitivity'], min_val = sensor['min_val'], max_val = sensor['max_val'])
            elif sensor['sensor_type'] == "Microphone" or sensor['sensor_type'] == "Sound":
                task.ai_channels.add_ai_force_iepe_chan(physical_channel = sensor['channel'], sensitivity = sensor['sensitivity'], min_val = sensor['min_val'], max_val = sensor['max_val'])
        # Setting sampling rate and samples per channel
        task.timing.cfg_samp_clk_timing(self.fs,sample_mode = nidaqmx.constants.AcquisitionType.CONTINUOUS, samps_per_chan=self.fs)
        
        self.__task__ = task

    
    def collect_data(self,fname='data.hdf5'):
        """
        Collects data
        
        Parameters
        ----------
        fname : str
            File that the data collected will be saved in (HDF5 file)
        
        """
        
        import tables
        import numpy as np
        from tqdm import tqdm
#       import progressbar #outdated function
        import sys
        import matplotlib.pyplot as plt
        
        class experiment(tables.IsDescription):
            """
            Defining the experiments table
            
            This table has the following columns:
                
                - parameter (string): The name of the parameter
                - value (duoble): Value of the parameter
                - units (string): Units of the parameter
                
            For example, for the frequency of acquisition would be:
                - parameter: sampling_frequency
                - value: 1652
                - units: Hz
            
            """
            parameter = tables.StringCol(120)
            value = tables.Float64Col()
            units = tables.StringCol(30)
        
        class sensors(tables.IsDescription):
            """
            Defining the sensors table
            
            This table has the following columns:
                
                - model (string): model of the sensor
                - serial (string): serial number of the sensor
                - sensitivity (float): sensitivity of the sensor
                - sensitivity_units (string): units of the sensitivity
                - units (string): units of the measurement after converted
                    to engineering units
                - location_x (float): location of the sensor (x-axis)
                - location_y (float): location of the sensor (y-axis)
                - location_z (float): location of the sensor (z-axis)
                - location_units (string): Units of the location of the sensor
                - direction_x (float): sensor direction (x-axis)
                - direction_y (float): sensor direction (y-axis)
                - direction_z (float): sensor direction (z-axis)
                - channel (string): Channel description from the DAQ
            """
            model = tables.StringCol(15)
            serial = tables.StringCol(15)
            sensitivity = tables.Float64Col()
            sensitivity_units = tables.StringCol(10)
            units = tables.StringCol(10)
            location_x = tables.Float64Col()
            location_y = tables.Float64Col()
            location_z = tables.Float64Col()
            location_units = tables.StringCol(10)
            direction_x = tables.Float64Col()
            direction_y = tables.Float64Col()
            direction_z = tables.Float64Col()
            channel = tables.StringCol(30)
            max_val = tables.Float64Col()
            min_val = tables.Float64Col()
            trigger = tables.BoolCol()           # True if this channel is used for triggering
            trigger_value = tables.Float64Col()
            sensor_type = tables.StringCol(30)   # Sensor type
            units = tables.StringCol(10)         # Units
        
        class record_parameters(tables.IsDescription):
            """
            Defining the table of the parameters per record
            
            The columns for this table are:
                - id (int): row of the record
                - parameter (string): parameter name
                - value (float): value of the parameter
                - units (float): units of the parameter
            """
            id = tables.IntCol()
            parameter = tables.StringCol(15)
            value = tables.StringCol(150)
            units = tables.StringCol(30)
            
        #%%
        class buffer:
            """
            Creates a buffer to be used in data collection
            #TODO: Complete help
            """
            pretrig = 0   # Number of points in the buffer for pre-trigger
            trigger = 0   # Amplitude used for trigger
            __buffer__ = np.array([])  # Buffer itself
            __position__ = 0 # Current position to add to the buffer
            
            def __init__(self,buffer_size, n_sensors = 1):
                """This class is an object to serve as a buffer for data collection.
                
                Args:
                    buffer_size (int): Number of points for the buffer. Corresponds to the
                        number of rows
                    n_sensors (int): Number of sensors in the buffer.  Corresponds to the number 
                        of columns.  Default value is 1.
                        
                Attributes:
                    buffer_size (int): Number of points for the buffer.
                    n_sensors (int): Number of sensors for the buffer.
                
                """
                self.__buffer__ = np.zeros((buffer_size, n_sensors))
                self.buffer_size = buffer_size
                self.n_sensors = n_sensors
                
            def add(self,data):
                """Adds data to a buffer object.
                
                Args:
                    data (np.array): Array to add to the buffer.  The size of data should
                    be n by self.n_sensors where n < self.buffer_size
                """
                # Check dimmensions of data and expand if needed
                if len(np.shape(data)) == 1:
                    data = np.expand_dims(data,axis=1)
                
                # Adding to the buffer
                if self.__position__ + np.shape(data)[0] > np.shape(self.__buffer__)[0]:
                    # If the data is bigger than the remaining of the buffer
                    points_end = np.shape(self.__buffer__)[0] - self.__position__
                    points_beginning = np.shape(data)[0] - points_end
                    self.__buffer__[self.__position__:,:] = data[0:points_end]
                    self.__buffer__[0:points_beginning,:] = data[points_end:]
                    self.__position__ = points_beginning
                else:    
                    # If the data is smaller than the remainder of the buffer
                    self.__buffer__[self.__position__:self.__position__+np.shape(data)[0],:] = data
                    self.__position__ += np.shape(data)[0]
        
            def read(self):
                """ Reads the buffer
                
                Returns:
                    data (np.array): Buffer in the correct order.
                """
                data = np.vstack((self.__buffer__[self.__position__:],self.__buffer__[0:self.__position__]))
                return data
        
        #%% Constants
        nsensors = len(self.sensors)

        #%% Opening hdf5 file in appebd mode
        efile = tables.open_file(fname, mode="a", title=self.title)
        
        # If group exists, opens existing group and adds to tables
        if "/experiment" not in efile:
            exp = efile.create_group("/", 'experiment', 'Experimental data')
        else:
            exp = efile.get_node('/experiment')
            
        if "/experiment/general_parameters" in efile:
            # opens table for experiment class (general parameters)
            gpar_table = efile.get_node('/experiment/general_parameters')
            gpar = gpar_table.row
        else:
            # creates table for experiment class (general parameters)
            gpar_table = efile.create_table(exp, 'general_parameters', experiment, "General Parameters Table")
            gpar = gpar_table.row
            gpar['parameter'] = 'fs'
            gpar['value'] = self.fs
            gpar['units'] = 'Hz'
            gpar.append()
            
            gpar['parameter'] = 'record_length'
            gpar['value'] = self.record_length
            gpar['units'] = 's'
            gpar.append()
            
            gpar['parameter'] = 'pre_trigger'
            gpar['value'] = self.pre_trigger
            gpar['units'] = 's'
            gpar.append()
            
            #%% Instead of adding parameters when not existing, the code should check if the parameters exist already in the table
            for parameter in self.parameters['general']:
                gpar['parameter'] = parameter['parameter']
                gpar['value'] = parameter['value']
                gpar['units'] = parameter['units']
                gpar.append()


        if "/experiment/sensors" in efile:                        
            # opens table for sensors class (sensor data)
            sensors_table = efile.get_node('/experiment/sensors')
            d_sensors = sensors_table.row        
        else:
            # creates table for sensors class (sensor data)
            sensors_table = efile.create_table(exp, 'sensors', sensors, "Sensors Table")
            d_sensors = sensors_table.row
            for sensor in self.sensors:
                d_sensors['model'] = sensor['model']
                d_sensors['serial'] = sensor['serial']
                d_sensors['sensitivity'] = int(sensor['sensitivity'])
                d_sensors['sensitivity_units'] = sensor['sensitivity_units']
                d_sensors['location_x'] = sensor['location'][0]
                d_sensors['location_y'] = sensor['location'][1]
                d_sensors['location_z'] = sensor['location'][2]
                d_sensors['location_units'] = sensor['location_units']
                d_sensors['direction_x'] = sensor['direction'][0]
                d_sensors['direction_y'] = sensor['direction'][1]
                d_sensors['direction_z'] = sensor['direction'][2]
                d_sensors['channel'] = sensor['channel']
                d_sensors['trigger'] = sensor['trigger']
                if sensor['trigger']:
                    d_sensors['trigger_value'] = sensor['trigger_value']
                else:
                    d_sensors['trigger_value'] = 0
                d_sensors['max_val'] = sensor['max_val']
                d_sensors['min_val'] = sensor['min_val']
                d_sensors['sensor_type'] = sensor['sensor_type']
                d_sensors['units'] = sensor['units']
                d_sensors.append()

        if "/experiment/specific_parameters" in efile:
            spar_table = efile.get_node('/experiment/specific_parameters')
            spar = spar_table.row      
        else:             
            spar_table = efile.create_table(exp, 'specific_parameters', record_parameters, "Specific Parameters Table")
            spar = spar_table.row
        
        if '/experiment/data' in efile:
            hdf5_data = efile.get_node('/experiment/data')
        else:
            hdf5_data = efile.create_earray(exp,'data',tables.Float64Atom(), shape=(0,nsensors,int(self.record_length*self.fs)))

        efile.flush()

        #%% Settup data acquisition and collect data
        print ('Setting up data acquisition system')
        self.setup_daq()
        
        # Defiing the buffer
        b = buffer((int(self.record_length+2)*self.fs),nsensors)
        
        # Determining what channels should be compared for trigger
        trigger_channs = [sensor['trigger'] for sensor in self.sensors]
        trigger_vals = [sensor['trigger_value'] for indx, sensor in zip(trigger_channs, self.sensors)]
        
        
        # Collecting data and adding it to the buffer
        triggered = False   # Bool to determine if the system was triggered in the past
        collect = True      # Bool to determine if the system should continue collecting
        extra_frames = 0    # Number of extra frames collected after trigger
        extra_frames_needed = int(np.ceil((self.record_length - self.pre_trigger)))+1
        print ('Collecting data ...')
        while collect:
            data = self.__task__.read(number_of_samples_per_channel=self.fs)  # Collecting 1 second
            data = np.array(data).transpose()
            b.add(data)
            # Testing if the signal is higher than the trigger
            maxvals = np.max(np.abs(data),axis = 0)
            if nsensors == 1:
                trigger = (maxvals > trigger_vals)[0]
            else:
                trigger = (maxvals[trigger_channs] > np.array(trigger_vals)[trigger_channs]).any()
            if trigger and not triggered:
                print ('Trigger detected, completting buffer ...')
                sys.stdout.flush()
                triggered = True
                
                bar = tqdm(total = extra_frames_needed, initial=1,unit='s', position=0, leave=False)
            if triggered:
                extra_frames += 1
                # print(str(extra_frames))
                bar.update()
                if extra_frames >= extra_frames_needed:
                    collect = False
                    # print('Finished')
                    bar.close
        
        # Stop and close the task
        self.__task__.stop()
        self.__task__.close()
        
        if input('plot? (y/n) ') == 'y':
        #%% Cuttine the record to the appropriate length
            record = b.read()
            trigger_location = []
            for chan_number in range(len(self.sensors)):
                if trigger_channs[chan_number]:
                    trigger_location.append(np.argmax(np.abs(record[:,chan_number]) > trigger_vals[chan_number]))
            trigger_location = min([tl for tl in trigger_location if tl > 0])
            start_record = int(trigger_location - np.round(self.fs*self.pre_trigger))
            end_record = int(start_record + (self.record_length * self.fs))
            record = record[start_record:end_record,:]
            print(len(record))
            #%% Plotting record
            t = np.arange(0,self.record_length,1/self.fs)
            plt.clf()
            # print('before plottin')
            plt.figure()
            for sensor, counter in zip(self.sensors,range(len(self.sensors))):
                plt.plot(t,record[:,counter], label = '%s'%sensor['serial'])
            plt.xlabel('Time (s)')
            plt.ylabel('Amplitude')
            plt.legend()
            plt.pause(0.1)
        
        #%% Ask if the record should be saved or not
        if input('Save this record? (y/n) ') == 'y':
            # Save the record in the hdf5 file
            hdf5_data.append(np.expand_dims(record.transpose(), axis = 0))
            data_id = hdf5_data.shape[0]-1
            for param in self.parameters['specific']:
                spar['id'] = data_id
                spar['parameter'] = param
                spar['value'] = input('What is the value for %s? '%(param))
                spar['units'] = input('Units for %s? '%(param))
                spar.append()

            efile.flush()

        
        #%%
                
        efile.close()       
        # Dumping general parameters to the table
        

    def load_setup(self,fname='setup.json'):
        """
        Opens the JSON file containing the setup parameters for the experiment.
        
        Parameters
        ----------
        fname : str
            File that the parameters for the experiment were saved into (JSON file)

        """
        import json
        
        with open(fname, 'r') as setup_file:
            setup_data = json.load(setup_file)
            
        self.fs = setup_data['sampling frequency']
        self.record_length = setup_data['record length']
        self.sensors = setup_data['sensors']
        self.parameters = setup_data['parameters']
        self.pre_trigger = setup_data['pre-trigger']
    
    def save_setup(self,fname='setup.json'):
        """
        Saves the configuration of an experiment to a json file
        
        Parameters
        ----------
        fname : str
            File that the parameters for the experiment will be stored into (JSON file)

        """
        import json
        # oraganizes all of the data collected
        datastore = {
            "sampling frequency": self.fs,
            "record length": self.record_length,
            "pre-trigger": self.pre_trigger,
            "sensors": self.sensors,
            "parameters": self.parameters
        }
        
        with open(fname, 'w') as outfile:
            json.dump(datastore, outfile)
    
    def setup(self):
        """
        Asks a series of questions to acquire information about the parameters 
        specific to the experiment. Then uses the save_setup definition to 
        save the parameters in a JSON file.

        """
        import json
        
        # brings the information from the json file containing the constants for the sensors
        with open('sensors.json', 'r') as accel:
            acc = json.load(accel)

        # Ask the experiment's title
        self.title = input('Title for this experiment? ')
        
        # Ask about the samplint frequency
        fs = input('What is the sampling frquency in Hz? (Default 1652) ')
        if fs: # If the string is not empty
            fs = int(fs)
        else:
            fs = 1652
        self.fs = fs
        
        # Asking about the pre-trigger
        self.pre_trigger = float(input('Pre_trigger length in seconds? '))
        
        # Ask about the record length
        self.record_length = float(input('Record length in seconds? '))
        
        # collects sensor data (model, serial number, sensitivity, location, direction, and channel)
        ns = input('How many sensors? ')
        for x in range(1, int(ns)+1):
            sensor={}
            flag = True
            while flag:
                try:        
                    sensor['model'] = input('What is the model of sensor %i? '%(x))
                    sensor['serial'] = input('What is the serial number of sensor %i? '%(x))
                    if acc[sensor['model']][sensor['serial']]['type'] == "Accelerometer" or acc[sensor['model']][sensor['serial']]['type'] == "Force" or acc[sensor['model']][sensor['serial']]['type'] == "Sound":
                        sensor['sensitivity'] = acc[sensor['model']][sensor['serial']]['sensitivity']
                        sensor['sensitivity_units'] = acc[sensor['model']][sensor['serial']]['sensitivity_units']
                        sensor['sensor_type'] = acc[sensor['model']][sensor['serial']]['type']
                        sensor['units'] = acc[sensor['model']][sensor['serial']]['units']
                    elif acc[sensor['model']][sensor['serial']]['type'] == "Hammer":
                        sensor['extender'] = input('no_extender or steel_extender? ')
                        sensor['sensitivity'] = acc[sensor['model']][sensor['serial']][sensor['extender']]['sensitivity']
                        sensor['sensitivity_units'] = acc[sensor['model']][sensor['serial']][sensor['extender']]['sensitivity_units']
                        sensor['sensor_type'] = acc[sensor['model']][sensor['serial']]['type']
                        sensor['units'] = acc[sensor['model']][sensor['serial']]['units']
                    else:
                        raise NameError('Unknown sensor type')
                    flag = False
                except KeyError:
                    print('Model and serial number do not match records. Please re-enter model and serial number.')
        # collect sensor position information
            loc_x = input('What is the x position of sensor %i? '%(x))
            loc_y = input('What is the y position of sensor %i? '%(x))
            loc_z = input('What is the z position of sensor %i? '%(x))
            loc_u = input('What are the location units of sensor %i? '%(x))
            loc = [float(loc_x), float(loc_y), float(loc_z)]    
            sensor['location'] = loc
            sensor['location_units'] = loc_u
        # collect sensor direction information
            dir_x = input('What is the x direction of sensor %i? '%(x))
            dir_y = input('What is the y direction of sensor %i? '%(x))
            dir_z = input('What is the z direction of sensor %i? '%(x))
            dire = [int(dir_x), int(dir_y), int(dir_z)]        
            sensor['direction'] = dire
        # Trigger level
            tmp = input('Trigger level for this sensor in %s? (Empty for no-trigger) '%sensor['units'])
            if tmp:
                sensor['trigger'] = True
                sensor['trigger_value'] = float(tmp) 
            else:
                sensor['trigger'] = False
            # Maximum and minimum value to record
            sensor['max_val'] = float(input('Maximum value to record in %s: '%sensor['units']))
            sensor['min_val'] = float(input('Minimum value to record in %s: '%sensor['units']))
            sensor['comments'] = input('Comments for this sensor? ')

        # channel information
            yn = 'n'
            while not(yn == 'y'):
                tmp = input('Tap sensor %i (you have 5 seconds after pressing enter)'%x)
                chan = identify_channel()
                yn = input('Is sensor %i (SN: %s) in %s? (y/n) '%(x,sensor['serial'],chan))
            sensor['channel'] = chan
            self.sensors.append(sensor)
            
        # gathers information about the general parameters of the experiment
        gen_par = input('How many general parameters will there be? ')
        general = []
        for x in range(1, int(gen_par)+1):
            gpa = input('What is general parameter %i? '%(x))
            gen={}
            gen['parameter'] = gpa
            gen['value'] = input('What is the value for ' + gpa + '? ')
            gen['units'] = input('What are the units for the ' + gpa + '? ')
            general.append(gen)
        
        # gathers information about the information specific to the data
        spe_par = input('How many specific parameters will there be? ')
        specific = []
        for x in range(1, int(spe_par)+1):
            spa = input('What is specific parameter %i? '%(x))
            specific.append(spa)
        
        # oraganizes all of the data collected
        self.parameters['general'] = general
        self.parameters['specific'] = specific

        self.save_setup()